#include <string>

#pragma once


class Question
{
private:
	std::string question;
public:
	Question(std::string value);
	std::string getQuestion();
};