#include <string>

#pragma once
class Answer
{
private:
	std::string answer;
public:
	Answer(std::string answer);
	std::string getAnswer();
};