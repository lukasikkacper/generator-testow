#include "QuestionSet.h"
#include <iostream>
#include <fstream>

void QuestionSet::loadFromFile() {

	std::string line;
	std::fstream file;

	file.open("pytania.txt", std::ios::in | std::ios::out);

	if (!file.good())
	{
		return;
	}

	while (!file.eof())
	{
		getline(file, line);
		questionList.insert(line);

	}

	file.close();

	file.open("odpowiedzi.txt", std::ios::in | std::ios::out);

	if (!file.good())
	{
		return;
	}

	while (!file.eof())
	{
		getline(file, line);
		answerList.insert(line);

	}
}
