#include "Question.h"
#include "Helpers.h"
#include <vector>

Question::Question(std::string value) {

	std::vector<std::string> result = Helpers::explode(value, ';');

	question = result.at(1);
}

std::string Question::getQuestion() {
	return question;
}